<?php

Class C_Dokter extends CI_Controller{

  function list_dokter(){
    $this->load->model('M_Dokter');
    $data["dokter"] = $this->M_Dokter->get_list_dokter();
    $this->load->view('template/header_view');
    $this->load->view('template/side_menu_admin');
    $this->load->view('dokter', $data);
    $this->load->view('template/footer');
  }

  function tambah_dokter(){
    $this->load->model('M_Dokter');
    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'no_telp' => $this->input->post('no_telp'),
      'email' => $this->input->post('email')
    );
    $this->M_Dokter->add_dokter($data);
  }

  function ubah_dokter(){
    $this->load->model('M_Dokter');
    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'no_telp' => $this->input->post('no_telp'),
      'email' => $this->input->post('email')
    );
    $id = $this->input->post('id');
    $this->M_Dokter->update_dokter($data,$id);
  }

  function non_aktifkan_dokter($id){
    $this->load->model('M_User');
    date_default_timezone_set('Asia/Jakarta');
    $date = date('Y-m-d H:i:s');
    $this->M_User->non_aktifkan_user($id, $date);
    redirect('C_Dokter/list_dokter/');
  }

  function aktifkan_dokter($id){
    $this->load->model('M_User');
    $this->M_User->aktifkan_user($id);
    redirect('C_Dokter/list_dokter/');
  }
}

 ?>
