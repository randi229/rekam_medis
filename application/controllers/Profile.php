<?php

Class Profile extends CI_Controller{

  public function __construct(){
    parent::__construct();
  }

  public function index(){
    if($this->session->userdata('status') == "login"){
      $this->load->model('M_Pasien');
      $data["pasien"]=$this->M_Pasien->get_pasien_by_id($this->session->userdata('id'));
      $this->load->view('template/header_view');
      if($this->session->userdata('role') == "pasien"){
        $this->load->view('template/side_menu_pasien');
      }else if($this->session->userdata('role') == "dokter"){
        $this->load->view('template/side_menu');
      }else if($this->session->userdata('role') == "Koordinator Klinik"){
        $this->load->view('template/side_menu_admin');
      }
      $this->load->view('profile2',$data);
      $this->load->view('template/footer.php');
    }else{
      redirect('Login');
    }
  }

  public function ubah_profile(){
    $this->load->model('M_Pasien');
    // validasi inputan
    $this->form_validation->set_rules('nama_lengkap', 'nama_lengkap', 'required|max_length[25]');
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    $this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin', 'required|max_length[12]');
    $this->form_validation->set_rules('tempat_lahir', 'tempat_lahir', 'required');
    $this->form_validation->set_rules('tgl_lahir', 'tgl_lahir', 'required');
    if($this->form_validation->run()==FALSE){
			 $this->session->set_flashdata('error',"Data Profile gagal diubah");
			 redirect('profile');
		}else{
      $data = array(
        'nama' => $this->input->post('nama_lengkap'),
        'jenis_kelamin' => $this->input->post('jenis_kelamin'),
        'gol_darah' => $this->input->post('gol_darah'),
        'tempat_lahir' => $this->input->post('tempat_lahir'),
        'tgl_lahir' => date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
        'alamat' => $this->input->post('alamat')
      );
      $this->M_Pasien->edit_pasien($data, $this->session->userdata('id'));
      $this->session->set_flashdata('sukses', "Data Profile behasil diubah");
      redirect('profile');
    }
  }



}


 ?>
