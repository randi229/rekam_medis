<?php

Class C_Staf extends CI_Controller{

  function list_staf(){
    $this->load->model('M_Staf');
    $data["staf"] = $this->M_Staf->get_list_staf();
    $this->load->view('template/header_view');
    $this->load->view('template/side_menu_admin');
    $this->load->view('staf',$data);
    $this->load->view('template/footer');
  }

  function tambah_staf(){
    $data = array(
      'nama' => $this->input->post('nama'),
      'alamat' => $this->input->post('alamat'),
      'no_telp' => $this->input->post('no_telp'),
      'email' => $this->input->post('email')
    );
    $this->load->model('M_Staf');
    $this->M_Staf->tambah_staf($data);
    redirect('C_Staf/list_staf/');
  }

  function ubah_staf(){

  }

  function hapus_staf($id){
    $this->load->model('M_Staf');
    $this->M_Staf->hapus_staf($id);
    redirect('C_Staf/list_staf/');
  }


}

 ?>
