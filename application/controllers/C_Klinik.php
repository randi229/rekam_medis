<?php

Class C_Klinik extends CI_Controller{

  function list_klinik(){
    $this->load->model('M_Klinik');
    $data["klinik"] = $this->M_Klinik->get_list_klinik();
    $this->load->view('template/header_view');
    $this->load->view('template/side_menu_admin');
    $this->load->view('klinik', $data);
    $this->load->view('template/footer');
  }

  function tambah_klinik(){
    $this->load->model('M_Klinik');
    $data = array(
      'kampus' => $this->input->post('kampus'),
      'alamat' => $this->input->post('alamat')
    );
    $this->M_Klinik->add_klinik($data);
    redirect('C_Klinik/list_klinik');
  }

  function ubah_klinik(){
    $this->load->model('M_Klinik');
    $data = array(
      'kampus' => $this->input->post('kampus'),
      'alamat' => $this->input->post('alamat')
    );
    $this->M_Klinik->update_klinik($data);
    redirect('C_Klinik/list_klinik');
  }

  function hapus_klinik($id){
    $this->load->model('M_Klinik');
    $this->M_Klinik->delete_klinik($id);
    redirect('C_Klinik/list_klinik');
  }
}


 ?>
