<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Pasien extends CI_Controller {

  // var $API = "";

  function __construct(){
    parent::__construct();
    // $this->API="http://localhost/api_rekmed/index.php/";
    // $this->load->library('curl');

  }

  public function rekam_medis($id=0)
  {
		if($id==0){
			redirect('pasien/page_not_found/', 'refresh');
		}else{
			$this->load->model('M_Pasien');
      $this->load->model('M_Rekam_Medis');
			$data["pasien"] = $this->M_Pasien->get_pasien_by_id($id);
      $data["rekam_medis"] = $this->M_Rekam_Medis->get_rekam_medis($id);
      // $data["pasien"] = json_decode($this->curl->simple_get($this->API.'pasien?id='.$id));
      // $data["rekam_medis"] = json_decode($this->curl->simple_get($this->API.'rekam_medis?id='.$id));
			$this->load->view('template/header_view');
			$this->load->view('template/side_menu');
			$this->load->view('rekam_medis', $data);
			$this->load->view('template/footer');
		}


  }

	public function list_pasien()
	{
		$this->load->model('M_Pasien');
		$data["pasien"] = $this->M_Pasien->get_all_pasien();
    // $data["pasien"] = json_decode($this->curl->simple_get($this->API.'pasien'));
		$this->load->view('template/header_view');
    if($this->session->userdata('role') == "staf"){
      $this->load->view('template/side_menu');
    }else if($this->session->userdata('role') == "Koordinator Klinik"){
      $this->load->view('template/side_menu_admin');
    }
    $this->load->view('pasien', $data);
    $this->load->view('template/footer');
	}

	public function tambah_pasien(){
		$this->form_validation->set_rules('email', 'email', 'required');
		if($this->form_validation->run()==FALSE){
			 $this->session->set_flashdata('error',"Data Gagal Di Tambahkan");
			 redirect('pasien/list_pasien');
		}else{
			$data=array(
        "id"=>$this->input->post('id'),
				"nama"=>$this->input->post('nama_lengkap'),
				"tempat_lahir"=>$this->input->post('tempat_lahir'),
				"tgl_lahir"=>date('Y-m-d', strtotime($this->input->post('tgl_lahir'))),
				"jenis_kelamin"=>$this->input->post('jenis_kelamin'),
				"alamat"=>$this->input->post('alamat'),
				"gol_darah"=>$this->input->post('gol_darah'),
        "email"=>$this->input->post('email')
			);
			$this->load->model('M_Pasien');
			$this->M_Pasien->add_pasien($data);
			$this->session->set_flashdata('sukses',"Data Berhasil Disimpan");
      redirect('pasien/list_pasien');
		}
	}


	public function page_not_found(){
		$this->load->view('template/404.php');
	}

}
