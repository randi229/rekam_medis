<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_User');
	}

	public function index()
	{
		if($this->session->userdata('status'))
    {
      if($this->session->userdata('role')=="pasien"){
				redirect('C_rekam_medis', 'refresh');
			}else if($this->session->userdata('role')=="dokter"){
				redirect('C_rekam_medis/cari_rekmed', 'refresh');
			}else if($this->session->userdata('role')=="Koordinator Klinik"){
				redirect('C_Dokter/list_dokter/', 'refresh');
			}
    }else
    {
      $this->load->helper(array('form'));
    	$this->load->view('login_view');
    }
	}

	public function cek_login(){
		$username = $this->input->post('username');
		// enkripsi password dengan menggunakan library encrypt codeigniter
		// $this->encrypt->encode() - enkripsi
		// $this->encrypt->decode() - dekripsi
		// untuk key nya disetting di config.php bagian $config['encryption_key']
		$password = $this->encryption->encrypt($this->input->post('password'));

		$cek = $this->M_User->get_user($username, $password);
		$login = $cek->row(1);
		if($cek->num_rows() > 0){
			$data = array(
				'id' => $login->nomor_induk,
				'nama' => $login->nama,
				'role' => $login->level,
				'status' => "login"
			);
			$this->session->set_userdata($data);
			if($login->level == "pasien"){
				redirect('C_rekam_medis');
			}else if($login->level == "dokter"){
				redirect('C_Rekam_medis/cari_rekmed');
			}else if($login->level == "staf"){
				redirect('C_Pasien/list_pasien/');
			}else if($login->level == "Koordinator Klinik"){
				redirect('C_Dokter/list_dokter/');
			}

		}else{
			echo "Username dan Password salah !".$password;

		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('Login');
	}


}
