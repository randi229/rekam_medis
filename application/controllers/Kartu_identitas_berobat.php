<?php

Class Kartu_identitas_berobat extends CI_Controller{

  public function __construct(){
    parent::__construct();
    $this->load->library('Pdf');
  }

  public function index(){
    $id = $this->input->post('id');
    $nama = $this->input->post('nama');
    $alamat = $this->input->post('alamat');
    $tempat_lahir = $this->input->post('tempat_lahir');
    $tgl_lahir = $this->input->post('tgl_lahir');
    $gol_darah = $this->input->post('gol_darah');
    $jenis_kelamin = $this->input->post('jenis_kelamin');
    $alergi_obat = $this->input->post('alergi_obat');
    if($nama==''){
      redirect('error', 'refresh');
    }
    // require_once(dirname(__FILE__).'/../libraries/tcpdf/example/barcode/tcpdf_barcodes_2d.php');
    $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // set document information
    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor('Klinik Universitas Pasundan');
    $pdf->SetTitle('Kartu Identitas Berobat');


    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' ', PDF_HEADER_STRING, array(0,0,0));
    // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);
    // set header and footer fonts
    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(5, 5, 5);
    // ---------------------------------------------------------
    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/../libraries/tcpdf/config/lang/eng.php');
        $pdf->setLanguageArray($l);
    }
    // set default font subsetting mode
    $pdf->setFontSubsetting(true);
    // set font
    $pdf->SetFont('times', 'B', 20);
    $pdf->SetAutoPageBreak(true, 20);
    $pdf->AddPage('L', 'A6');
    // Set some content to print
    $style = array(
      'border' => 2,
      'vpadding' => 'auto',
      'hpadding' => 'auto',
      'fgcolor' => array(0,0,0),
      'bgcolor' => false, //array(255,255,255)
      'module_width' => 1, // width of a single module in points
      'module_height' => 1 // height of a single module in points
    );
    $barcode = $pdf->serializeTCPDFtagParameters(array($id, 'QRCODE,H', 100, 25, 40, 40, $style, 'N'));
    // set the barcode content and type
    // $barcodeobj = new TCPDF2DBarcode('http://www.tcpdf.org', 'QRCODE,H');
    $html =
    '<style>
    table {
      border-collapse: collapse;
    }

    table{
      padding-top: -20px;
    }

    table, th, td {
      font-family: "Times New Roman";
      font-size: 8px;
      padding: 5px;
    }

    p{
       font-family: "Times New Roman";
       margin-bottom: -20px;
    }
    </style>
    <p style="font-size: 8px; margin-top: -20px; text-align: center;">
      <b style="font-size: 10px;">KARTU IDENTITAS BEROBAT</b><br>
      <b>KLINIK</b><br>
      <b>UNIT UNIVERSITAS PASUNDAN</b><br>
      Jalan Tamansari No.6-8 Bandung
      Tlp. (022) 4218060 - 4205832 Fax. 436162
    </p>
    <table>
      <tr>
        <td>Nama</td>
        <td style="width: 15px;">:</td>
        <td style="width: 145px;">'.$nama.'</td>
        <td style="width: 130px;" rowspan="6"><tcpdf method="write2DBarcode" params="'.$barcode.'" /></td>
      </tr>
      <tr>
        <td>Jenis Kelamin</td>
        <td style="width: 15px;">:</td>
        <td>'.$jenis_kelamin.'</td>
      </tr>
      <tr>
        <td>Tempat, Tgl. Lahir</td>
        <td style="width: 15px;">:</td>
        <td >'.$tempat_lahir.', '.$tgl_lahir.'</td>
      </tr>
      <tr>
        <td>Alamat</td>
        <td>:</td>
        <td>'.$alamat.'</td>
      </tr>
      <tr>
        <td>Alergi Obat</td>
        <td>:</td>
        <td>'.$alergi_obat.'</td>
      </tr>
      <tr>
        <td >Gol. Darah</td>
        <td style="width: 15px;">:</td>
        <td>'.$gol_darah.'</td>
      </tr>
      <tr>
        <td colspan="6" style="text-align:center; font-size: 7px;">
          <b>DOKTER JAGA</b><br>
          Buka Setiap Hari Senin s/d Sabtu<br>
          Jam 09.00 - 14.00 WIB<br>
          Harap Dibawa Setiap Berobat
        </td>
      </tr>
    </table>
    ';



    // Print text using writeHTMLCell()
    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage('L', 'A6');
    $subtable = '
    <table class="tabel">
      <tr class="tabel">
        <td class="tabel" style="width: 23px;">No.</td>
        <td class="tabel">Tgl. Berobat</td>
        <td class="tabel">Paraf</td>
        <td class="tabel" style="width: 70px;">Ket.</td>
      </tr>
      <tr class="tabel">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr class="tabel">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr class="tabel">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
      <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>';
    $html ='<style>
    table {
      border-collapse: collapse;
    }

    table{
      padding-top: -20px;
    }

    table, th, td {
      font-family: "Times New Roman";
      font-size: 8px;
      padding: 5px;
      border: 1px solid black;
    }

    p{
       font-family: "Times New Roman";
       margin-bottom: -20px;
    }
    </style>
    <table style="border:hidden;">
      <tr style="border:hidden;">
        <td style="border:hidden;">'.$subtable.'</td>
        <td style="border:hidden;">'.$subtable.'</td>
      </tr>

    </table>';
    $pdf->writeHTML($html, true, false, true, false, '');
    // reset pointer to the last page
    $pdf->lastPage();
    //Close and output PDF document
    $pdf->Output('Kartu Identitas Berobat.pdf', 'I');

  }

}

 ?>
