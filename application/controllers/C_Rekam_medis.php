<?php

Class C_Rekam_medis extends CI_Controller{

  function __construct(){
    parent::__construct();

  }
  // function _remap($id=0) {
  //       $this->index($id);
  // }

  function index($id=0){
    $this->load->model('M_Rekam_medis');
    if($this->session->userdata('status') != "login"){
			redirect('Login');
		}
    $this->load->view('template/header_view');
    $this->load->view('template/side_menu_pasien');
    if($id==0){
      $data["rekam_medis"] = $this->M_Rekam_medis->get_rekam_medis_by_id($this->session->userdata('id'));
      $this->load->view('rekam_medis_pasien', $data);
    }else{
      $this->load->model('M_Pasien');
      $data["pasien"] = $this->M_Pasien->get_pasien_by_id($id);
      $data["rekam_medis"] = $this->M_Rekam_medis->get_rekam_medis($id);
      $this->load->view('rekam_medis', $data);
    }
    $this->load->view('template/footer');
  }

  function tambah_catatan(){
    $this->load->model('M_Rekam_medis');
    $data = array(
      'tgl' => date('Y-m-d'),
      'diagnosis' => $this->input->post('diagnosis'),
      'terapi' => $this->input->post('terapi'),
      'id_dokter' => $this->session->userdata('id')
    );
    $this->M_Rekam_medis->add_rekam_medis($data);
  }

  function ubah_catatan(){
    $this->load->model('M_Rekam_Medis');
    $id = $this->input->post('id_catatan');
    $id_pasien = $this->input->post('id');
    $data = array(
      'diagnosis' => $this->input->post('diagnosis'),
      'terapi' => $this->input->post('terapi')
    );
    $this->M_Rekam_Medis->edit_rekam_medis_by_id($data, $id);
    redirect('pasien/rekam_medis/'.$id_pasien);
  }

  function rekap(){
    $this->load->model('M_Rekam_Medis');
    $data["rekap"] = $this->M_Rekam_Medis->get_rekap();
    $this->load->view('template/header_view');
    $this->load->view('template/side_menu_admin');
    $this->load->view('rekap',$data);
    $this->load->view('template/footer');
  }

  public function cari_rekmed(){
    $this->load->view('template/header_view');
    $this->load->view('template/side_menu');
    $this->load->view('cari_rekmed');
    $this->load->view('template/footer_cari_rekmed');
  }

}

 ?>
