<?php

Class M_Pasien extends CI_Model{

  public function get_all_pasien(){
    $this->db->select('profile.nomor_induk, .profile.nama, jenis_kelamin, alamat, tempat_lahir, tgl_lahir, gol_darah, alergi_obat');
    $this->db->from('user');
    $this->db->join('profile', 'user.profile_id = profile.nomor_induk');
    $this->db->where('user.role', 1);
    $query=$this->db->get();
    return $query->result();
  }

  public function get_pasien_by_id($id){
    $this->db->select('nomor_induk, nama, jenis_kelamin, alamat, tempat_lahir, tgl_lahir, gol_darah, alergi_obat');
    $this->db->from('profile');
    $this->db->where('nomor_induk', $id);
    $query=$this->db->get();
    return $query->result();
  }

  public function add_pasien($data){
    $this->db->insert('profile', $data);
  }

  public function edit_pasien($data, $id){
    $this->db->set($data);
    $this->db->where('nomor_induk', $id);
    $this->db->update('profile');
  }

}

 ?>
