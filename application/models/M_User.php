<?php

Class M_User extends CI_Model{

  public function get_user($username, $password){
    $this->db->select('user.username, role.role as level, profile.nomor_induk ,profile.nama');
    $this->db->from('user');
    $this->db->join('profile', 'user.profile_id = profile.nomor_induk');
    $this->db->join('role', 'user.role = role.id');
    $this->db->where('username', $username);
    $this->db->where('password', $this->encryption->decrypt($password));
    $this->db->where('deleted_at', NULL);
    $query = $this->db->get();
    return $query;
  }

  public function edit_password($username, $password){
    $this->db->set('password', $password);
    $this->db->where('username', $username);
    $this->db->update('user');
  }

  public function non_aktifkan_user($id, $date){
    $this->db->set('deleted_at', $date);
    $this->db->where('id', $id);
    $this->db->update('user');
  }

  public function aktifkan_user($id){
    $this->db->set('deleted_at', NULL);
    $this->db->where('id', $id);
    $this->db->update('user');
  }

}

 ?>
