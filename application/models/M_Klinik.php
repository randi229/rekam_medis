<?php

Class M_Klinik extends CI_Model{

  function get_list_klinik(){
    $this->db->select('id, kampus, alamat');
    $this->db->from('klinik');
    $query = $this->db->get();
    return $query->result();
  }

  function add_klinik($data){
    $this->db->insert('klinik',$data);
  }

  function update_klinik($data,$id){
    $this->db->set($data);
    $this->db->where($id);
    $this->db->update('klinik');
  }

  function delete_klinik($id){
    $this->db->where('id',$id);
    $this->db->delete('klinik');
  }

}

 ?>
