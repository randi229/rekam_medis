<?php

Class M_Dokter extends CI_Model{

  function get_list_dokter(){
    $this->db->select('user.id, user.profile_id, user.deleted_at, profile.nama, profile.jenis_kelamin, profile.alamat, profile.no_telp, profile.email');
    $this->db->from('user');
    $this->db->join('profile', 'user.profile_id = profile.nomor_induk');
    $this->db->where('user.role', 2);
    $query = $this->db->get();
    return $query->result();
  }

  function get_dokter_by_id($id){
    $this->db->select('id, nama, jenis_kelamin, alamat, no_telp, email');
    $this->db->from('dokter');
    $this->db->where('id', $id);
    $query = $this->db->get();
    return $query->result();
  }

  function add_dokter($data){
    $this->db->insert('dokter', $data);
  }

  function update_dokter($data, $id){
    $this->db->set($data);
    $this->db->where($id);
    $this->db->update('klinik');
  }

  function delete_dokter($id){
    $this->db->where('id', $id);
    $this->db->delete('dokter');
  }


}
?>
