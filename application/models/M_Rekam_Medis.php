<?php

Class M_Rekam_Medis extends CI_Model{

  public function get_rekam_medis_by_id($id){
    $this->db->select('rekam_medis.tgl,rekam_medis.diagnosis, rekam_medis.terapi, profile.nama as dokter');
    $this->db->from('rekam_medis');
    $this->db->join('profile', 'rekam_medis.id_dokter = profile.nomor_induk');
    $this->db->where('rekam_medis.id_pasien', $id);
    $query=$this->db->get();
    return $query->result();
  }

  public function get_rekam_medis($id){
    $this->db->select('rekam_medis.id, rekam_medis.tgl, rekam_medis.diagnosis, rekam_medis.terapi, profile.nama as dokter');
    $this->db->from('rekam_medis');
    $this->db->join('profile', 'rekam_medis.id_dokter = profile.nomor_induk');
    $this->db->where('rekam_medis.id_pasien', $id);
    $query=$this->db->get();
    return $query->result();
  }

  public function add_rekam_medis($data){
    $this->db->insert('rekam_medis', $data);
  }

  public function edit_rekam_medis_by_id($data, $id){
    $this->db->set($data);
    $this->db->where('id', $id);
    $this->db->update('rekam_medis');
  }

  public function get_rekap(){
    $this->db->select('rekam_medis.tgl, pasien.nama as pasien, rekam_medis.diagnosis, rekam_medis.terapi, dokter.nama as dokter');
    $this->db->from('rekam_medis');
    $this->db->join('profile pasien', 'rekam_medis.id_pasien = pasien.nomor_induk');
    $this->db->join('profile dokter', 'rekam_medis.id_dokter = dokter.nomor_induk');
    $query=$this->db->get();
    return $query->result();
  }
}

 ?>
