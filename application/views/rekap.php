<div class="right_col" role="main">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Rekap Pasien Klinik</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table class="table table-striped table-bordered" id="datatable">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Dokter</th>
                <th>Nama Pasien</th>
                <th>Diagnosa</th>
                <th>Terapi</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $i = 1;
                foreach ($rekap as $data) {
                  echo "<tr>
                          <td>".$i."</td>
                          <td>".$data->tgl."</td>
                          <td>".$data->dokter."</td>
                          <td>".$data->pasien."</td>
                          <td>".$data->diagnosis."</td>
                          <td>".$data->terapi."</td>
                        </tr>";
                        $i++;
                }
               ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
