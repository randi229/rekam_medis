<div class="right_col" role="main">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Klinik</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                  </button>
                  <h4 class="modal-title" id="myModalLabel2">Tambah Klinik</h4>
                </div>
                <?php echo form_open('C_Klinik/tambah_klinik','class ="form-horizontal form-label-left"'); ?>
                <div class="modal-body">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3" for="kampus">Kampus</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <input type="text" name="kampus" class="form-control" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3" for="alamat">Alamat</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                      <textarea name="alamat" class="form-control"></textarea>
                    </div>
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <input type="submit" name="simpan" value="Simpan" class="btn btn-primary">
                </div>
              </div>
              <?php echo form_close(); ?>
            </div>
          </div>
          <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bs-example-modal-sm">Tambah Klinik</button>
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Kampus</th>
                <th>Alamat</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $i = 1;
                foreach ($klinik as $k) {
                  echo "<tr>
                          <td>".$i."</td>
                          <td>".$k->kampus."</td>
                          <td>".$k->alamat."</td>
                          <td><button class='btn btn-warning btn-sm'><i class='fa fa-edit'></i>Ubah</button>".anchor('C_Klinik/hapus_klinik/'.$k->id, '<i class= "fa fa-trash"> Hapus', array('class' => 'btn btn-danger btn-sm'))."</td>
                        </tr>";
                  $i++;
                }

               ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
