<div class="right_col" role="main">

        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Profil Pasien</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <!-- content starts here -->
                        <div class="row">
                          <?php
                          $id = "";
                          $nama = "";
                          $jenis_kelamin = "";
                          $alamat = "";
                          $tempat_lahir = "";
                          $tgl_lahir = "";
                          $gol_darah = "";
                          $alergi_obat = "";
                          foreach ($pasien as $pas) {
                            $id = $pas->nomor_induk;
                            $nama = $pas->nama;
                            $jenis_kelamin = $pas->jenis_kelamin;
                            $alamat = $pas->alamat;
                            $tempat_lahir = $pas->tempat_lahir;
                            $tgl_lahir = $pas->tgl_lahir;
                            $gol_darah = $pas->gol_darah;
                            $alergi_obat = $pas->alergi_obat;
                          }  ?>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table profil-pasien">
                              <tbody>
                                <tr>
                                  <td>Nama</td>
                                  <td>:</td>
                                  <td><?php echo $nama; ?></td>
                                </tr>
                                <tr>
                                  <td>Jenis Kelamin</td>
                                  <td>:</td>
                                  <td><?php echo $jenis_kelamin; ?></td>
                                </tr>
                                <tr>
                                  <td>Alergi Obat</td>
                                  <td>:</td>
                                  <td><?php echo $alergi_obat; ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table profil-pasien">
                              <tr>
                                <td>Golongan Darah</td>
                                <td>:</td>
                                <td><?php echo $gol_darah; ?></td>
                              </tr>
                              <tr>
                                <td>Tempat tgl. lahir</td>
                                <td>:</td>
                                <td><?php echo $tempat_lahir. ", " . date('d-m-Y', strtotime($tgl_lahir)); ?></td>
                              </tr>
                              <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?php echo $alamat; ?></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <?php
                        $data = array(
                          'id' => $id,
                          'nama' =>$nama,
                          'jenis_kelamin' =>$jenis_kelamin,
                          'gol_darah' => $gol_darah,
                          'tempat_lahir' => $tempat_lahir,
                          'tgl_lahir' => $tgl_lahir,
                          'alamat' => $alamat,
                          'alergi_obat' => $alergi_obat
                        );
                        echo form_open('Kartu_identitas_berobat', '', $data); ?>
                          <input type="submit" class="btn btn-success pull-left" value="Cetak Kartu">
                        </form>
                        <!-- content ends here -->
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
              <div class="x_title">
                  <h2>Rekam Medis</h2>
                  <div class="clearfix"></div>
              </div>
              <div class="x_content">
                <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-plus"></i> Rekam Medis</button>
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">Rekam Medis</h4>
                      </div>

                      <?php
                      echo form_open('C_rekam_medis/tambah_catatan','class ="form-horizontal form-label-left"'); ?>
                      <div class="modal-body">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="diagnosis">Diagnosis<span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="diagnosis" class="form-control" required></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Terapi">Terapi<span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="terapi" class="form-control" required></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value = 'simpan'>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>

                <table id="datatable" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>Tanggal</th>
                      <th>Diagnosis</th>
                      <th>Terapi</th>
                      <th>Dokter</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($rekam_medis as $rekmed){
                        echo "<tr>
                                <td>".date('d-m-Y', strtotime($rekmed->tgl))."</td>
                                <td>$rekmed->diagnosis</td>
                                <td>$rekmed->terapi</td>
                                <td>$rekmed->dokter</td>
                                <td><button class='btn btn-info btn-xs' data-toggle='modal' data-target='.modal-ubah".$rekmed->id."'><i class='fa fa-edit'></i> Ubah</button></td>
                              </tr>";
                    }
                    ?>
                  </tbody>
                </table>
                <?php
                  foreach ($rekam_medis as $rekmed){
                    $id_catatan = $rekmed->id;
                    $diagnosis = $rekmed->diagnosis;
                    $terapi = $rekmed->terapi;
                ?>
                <div class="modal fade modal-ubah<?php echo $id_catatan; ?>" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="myModalLabel">Rekam Medis</h4>
                      </div>

                      <?php
                      echo form_open('C_rekam_medis/ubah_catatan','class ="form-horizontal form-label-left"'); ?>
                      <div class="modal-body">
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="diagnosis">Diagnosis<span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" value="<?php echo $id; ?>" name="id" ?>
                            <input type="hidden" value="<?php echo $id_catatan; ?>" name="id_catatan" ?>
                            <textarea name="diagnosis" class="form-control" required><?php echo $diagnosis; ?></textarea>
                          </div>
                        </div>
                        <div class="form-group">
                          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Terapi">Terapi<span class="required">*</span></label>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <textarea name="terapi" class="form-control" required><?php echo $terapi; ?></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value = 'simpan'>
                      </div>
                      </form>
                    </div>
                  </div>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
</div>
</div>

</div>
</body>
