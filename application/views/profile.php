<div class="right_col" role="main">
        <?php echo $this->session->userdata('level'); ?>
        <div class="clearfix"></div>
        <?php echo validation_errors(); ?>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Profil Pasien</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <!-- content starts here -->
                        <div class="row">
                          <?php
                          $nama = "";
                          $jenis_kelamin = "";
                          $alamat = "";
                          $tempat_lahir = "";
                          $tgl_lahir = "";
                          $gol_darah = "";
                          $alergi_obat = "";
                          foreach ($pasien as $pas) {
                            $nama = $pas->nama;
                            $jenis_kelamin = $pas->jenis_kelamin;
                            $alamat = $pas->alamat;
                            $tempat_lahir = $pas->tempat_lahir;
                            $tgl_lahir = $pas->tgl_lahir;
                            $gol_darah = $pas->gol_darah;
                            $alergi_obat = $pas->alergi_obat;
                          }
                          $a = "";
                          $b = "";
                          $ab = "";
                          $o = "";
                          if($gol_darah =="A"){
                            $a = "checked";
                          }else if($gol_darah =="B"){
                            $b = "checked";
                          }else if($gol_darah =="AB"){
                            $ab = "checked";
                          }else if($gol_darah =="O"){
                            $o = "checked";
                          }

                          $laki = "";
                          $perempuan ="";
                          if($jenis_kelamin =="laki-laki"){
                            $laki = "checked";
                          }else if($jenis_kelamin == "perempuan"){
                            $perempuan = "checked";
                          }  ?>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php
                            $data=$this->session->flashdata('sukses');
                            if($data!=""){
                              echo "<script type='text/javascript'>
                                        $(document).ready(function(e) {
                                            notifEditProfile('success');
                                        });
                                    </script>";
                            }

                            $data2=$this->session->flashdata('error');
                            if($data2!=""){
                              echo "<script type='text/javascript'>
                                        $(document).ready(function(e) {
                                            notifEditProfile('error');
                                        });
                                    </script>";
                            }
                            ?>
                            <table class="table profil-pasien">
                              <tbody>
                                <tr>
                                  <td>Nama</td>
                                  <td>:</td>
                                  <td><?php echo $nama; ?></td>
                                </tr>
                                <tr>
                                  <td>Jenis Kelamin</td>
                                  <td>:</td>
                                  <td><?php echo $jenis_kelamin; ?></td>
                                </tr>
                                <tr>
                                  <td>Alergi Obat</td>
                                  <td>:</td>
                                  <td><?php echo $alergi_obat; ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <table class="table profil-pasien">
                              <tr>
                                <td>Golongan Darah</td>
                                <td>:</td>
                                <td><?php echo $gol_darah; ?></td>
                              </tr>
                              <tr>
                                <td>Tempat tgl. lahir</td>
                                <td>:</td>
                                <td><?php echo $tempat_lahir. ", " . date('d-m-Y', strtotime($tgl_lahir)); ?></td>
                              </tr>
                              <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td><?php echo $alamat; ?></td>
                              </tr>
                            </table>
                          </div>
                        </div>
                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-edit"></i> Ubah Profile</button>
                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                          <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                                <h4 class="modal-title" id="myModalLabel">Ubah Profile</h4>
                              </div>

                              <?php echo form_open('profile/ubah_profile/','class ="form-horizontal form-label-left"'); ?>
                              <div class="modal-body">
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_lengkap">Nama Lengkap<span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="nama_lengkap" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo $nama; ?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ttl">Tempat, Tanggal lahir<span class="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="tempat_lahir" required="required" class="form-control col-md-3 col-xs-12" style="width: 40%; margin-right: 10px;" value="<?php echo $tempat_lahir; ?>">
                                    <div class='input-group date' id='myDatepicker2'>
                                      <input type='text' class="form-control" name="tgl_lahir" value="<?php echo date('d-m-Y', strtotime($tgl_lahir)); ?>" />
                                      <span class="input-group-addon">
                                       <span class="glyphicon glyphicon-calendar"></span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_kelamin">Jenis Kelamin<span="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="gender" class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="jenis_kelamin" value="laki-laki" <?php echo $laki; ?>/> &nbsp; Laki - laki &nbsp;
                                      </label>
                                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="jenis_kelamin" value="perempuan" <?php echo $perempuan; ?>/> Perempuan
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat<span="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea id="message" required="required" class="form-control" name="alamat"><?php echo $alamat; ?></textarea>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan_darah">Golongan Darah<span="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="golongan_darah" class="btn-group" data-toggle="buttons">
                                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="gol_darah" value="A" <?php echo $a; ?>> A
                                      </label>
                                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="gol_darah" value="B" <?php echo $b; ?>> B
                                      </label>
                                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="gol_darah" value="AB" <?php echo $ab; ?>> AB
                                      </label>
                                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                        <input type="radio" name="gol_darah" value="O" <?php echo $o; ?>> O
                                      </label>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alergi_obat">Alergi Obat<span="required">*</span>
                                  </label>
                                  <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="alergi_obat" class="form-control col-md-7 col-xs-12" value="<?php echo $alergi_obat; ?>">
                                  </div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value = 'simpan'>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                        <!-- content ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

</div>
</body>
