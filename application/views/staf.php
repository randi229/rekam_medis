<div class="right_col" role="main">
  <div class="clearfix"></div>
  <div class="page-title">
    <div class="title-left">
      <h3>Staf</h3>
    </div>
  </div>
  <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
          <h4 class="modal-title" id="myModalLabel">Tambah Staf</h4>
        </div>

        <?php echo form_open('C_Staf/tambah_staf','class ="form-horizontal form-label-left"'); ?>
        <div class="modal-body">
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_lengkap">Nama Lengkap<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="nama_lengkap" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">NRP/NIS<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="id" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ttl">Tempat, Tanggal lahir<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="tempat_lahir" required="required" class="form-control col-md-3 col-xs-12" style="width: 40%; margin-right: 10px;">
              <div class='input-group date' id='myDatepicker2'>
                <input type='text' class="form-control" name="tgl_lahir" />
                <span class="input-group-addon">
                 <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_kelamin">Jenis Kelamin<span="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div id="gender" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="jenis_kelamin" value="laki-laki"> &nbsp; Laki - laki &nbsp;
                </label>
                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="jenis_kelamin" value="perempuan"> Perempuan
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat<span="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea id="message" required="required" class="form-control" name="alamat"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan_darah">Golongan Darah<span="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <div id="golongan_darah" class="btn-group" data-toggle="buttons">
                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="gol_darah" value="A"> A
                </label>
                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="gol_darah" value="B"> B
                </label>
                <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="gol_darah" value="AB"> AB
                </label>
                <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                  <input type="radio" name="gol_darah" value="O"> O
                </label>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alergi_obat">Alergi Obat<span="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="alergi_obat" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <input type="submit" class="btn btn-success" value = 'simpan'>
        </div>
        </form>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>List Staf</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bs-example-modal-lg">Tambah Staf</button>
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>No.Telp</th>
                <th>Email</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $j = 1;
              foreach ($staf as $data) {
                echo "<tr>
                      <td>$j</td>
                      <td>$data->nama</td>
                      <td>$data->jenis_kelamin</td>
                      <td>$data->alamat</td>
                      <td>$data->no_telp</td>
                      <td>$data->email</td>
                      <td>".anchor('C_Dokter/hapus_dokter/'.$data->profile_id, '<i class= "fa fa-trash"> Hapus', array('class' => 'btn btn-danger btn-sm'))."</td>
                      </tr>";
                      $j++;
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
