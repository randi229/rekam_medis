<div class="right_col" role="main">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Rekam Medis</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <table id="datatable" class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Tanggal</th>
                <th>Diagnosis</th>
                <th>Terapi</th>
                <th>Dokter</th>
              </tr>
            </thead>
            <tbody>
              <?php
              foreach ($rekam_medis as $rekmed){
                echo "<tr>
                        <td>".date('d-m-Y', strtotime($rekmed->tgl))."</td>
                        <td>$rekmed->diagnosis</td>
                        <td>$rekmed->terapi</td>
                        <td>$rekmed->dokter</td>
                      </tr>";
              }
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</div>
</div>
</div>
</body>
