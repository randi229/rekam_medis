<div class="right_col" role="main">

    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>
                    Pasien
                </h3>
            </div>
        </div>
        <?php
        $data=$this->session->flashdata('sukses');
        if($data!=""){
          echo "<script type='text/javascript'>
                    $(document).ready(function(e) {
                        notifTambahPasien('success');
                    });
                </script>";
        }

        $data2=$this->session->flashdata('error');
        if($data2!=""){
          echo "<script type='text/javascript'>
                    $(document).ready(function(e) {
                        notifTambahPasien('error');
                    });
                </script>";
        }
        ?>



        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="myModalLabel">Tambah Pasien</h4>
              </div>

              <?php echo form_open('pasien/tambah_pasien','class ="form-horizontal form-label-left"'); ?>
              <div class="modal-body">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama_lengkap">Nama Lengkap<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="nama_lengkap" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="id">NRP/NIS<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="id" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ttl">Tempat, Tanggal lahir<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="tempat_lahir" required="required" class="form-control col-md-3 col-xs-12" style="width: 40%; margin-right: 10px;">
                    <div class='input-group date' id='myDatepicker2'>
                      <input type='text' class="form-control" name="tgl_lahir" />
                      <span class="input-group-addon">
                       <span class="glyphicon glyphicon-calendar"></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_kelamin">Jenis Kelamin<span="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="gender" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="jenis_kelamin" value="laki-laki"> &nbsp; Laki - laki &nbsp;
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="jenis_kelamin" value="perempuan"> Perempuan
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat<span="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <textarea id="message" required="required" class="form-control" name="alamat"></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan_darah">Golongan Darah<span="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <div id="golongan_darah" class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="gol_darah" value="A"> A
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="gol_darah" value="B"> B
                      </label>
                      <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="gol_darah" value="AB"> AB
                      </label>
                      <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                        <input type="radio" name="gol_darah" value="O"> O
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alergi_obat">Alergi Obat<span="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" name="alergi_obat" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">Email<span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="email" name="email" required="required" class="form-control col-md-7 col-xs-12">
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <input type="submit" class="btn btn-success" value = 'simpan'>
              </div>
              </form>
            </div>
          </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>List Pasien</h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <!-- content starts here -->
                        <button type="button" class="btn btn-dark" data-toggle="modal" data-target=".bs-example-modal-lg">Tambah Pasien</button>
                        <table id="datatable" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Nama</th>
                              <th>Jenis kelamin</th>
                              <th>Alamat</th>
                              <th>Tempat tgl. lahir</th>
                              <th>Gol. Darah</th>
                              <th>Alergi Obat</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php
                            $j = 1;
                            foreach ($pasien as $pas) {
                              echo "<tr>
                                    <td>$j</td>
                                    <td>$pas->nama</td>
                                    <td>$pas->jenis_kelamin</td>
                                    <td>$pas->alamat</td>
                                    <td>$pas->tempat_lahir, ".date('d-m-Y', strtotime($pas->tgl_lahir))."</td>
                                    <td>$pas->gol_darah</td>
                                    <td>$pas->alergi_obat</td>
                                    <td>".anchor('C_Pasien/rekam_medis/'.$pas->nomor_induk, '<i class= "fa fa-folder"> Lihat Rekam Medis', array('class' => 'btn btn-primary btn-xs'))."</td>
                                    </tr>";
                                    $j++;
                            }
                            ?>
                          </tbody>
                        </table>
                        <!-- content ends here -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

</div>
</body>
