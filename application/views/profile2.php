<div class="right_col" role="main">
  <div class="clearfix"></div>
  <div class="row">
    <div class="col-md-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Profile</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <?php echo form_open('profile/ubah_profile/','class ="form-horizontal form-label-left"'); ?>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="nama">Nama Lengkap</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="nama" value="" class="form-control">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="jenis_kelamin">Jenis Kelamin</label>
            <div class="col-md-6 col-sm-6 col-xs-12">

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ttl">Tempat, Tanggal Lahir</label>
            <div class="col-md-6 col-sm-6 col-xs-12">

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alamat">Alamat</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea name="alamat" class="form-control" ></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="golongan_darah">Golongan Darah</label>
            <div class="col-md-6 col-sm-6 col-xs-12">

            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="alergi_obat">Alergi_Obat</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea name="alergi_obat" class="form-control"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="no_telp">No. Telp</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="no_telp" class="form-control" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="ttl">Email</label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="email" name="email" class="form-control" value="">
            </div>
          </div>
          <?php echo form_close(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
