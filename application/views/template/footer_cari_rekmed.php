<!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-datetimepicker -->
    <script src="<?php echo base_url(); ?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- CkEditor -->
    <script src="<?php echo base_url(); ?>assets/vendors/ckeditor/ckeditor.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>
    <!-- combobox -->
    <script src="<?php echo base_url(); ?>assets/combobox-master/combobox.js"></script>
    <!-- pnotify -->
    <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <!-- webcodecamjs -->
    <script src="<?php echo base_url(); ?>assets/webcodecamjs/js/qrcodelib.js"></script>
    <script src="<?php echo base_url(); ?>assets/webcodecamjs/js/webcodecamjs.js"></script>
    <script src="<?php echo base_url(); ?>assets/webcodecamjs/js/main.js"></script>
    <script>
    $("#submit-id").click(function(){
      var id = $("#id_pasien").val();
      $.ajax({
        type: "POST",
        url: "http://localhost/cis/index.php/chat/submit",
        data: id,
        dataType: "json",
        cache: false,
        success : function(data){

        }
      });
    });
    </script>
  </body>
</html>
