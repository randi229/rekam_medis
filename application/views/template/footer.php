<!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>


    <!-- Bootstrap -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url(); ?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url(); ?>assets/vendors/nprogress/nprogress.js"></script>
    <!-- bootstrap-datetimepicker -->
    <script src="<?php echo base_url(); ?>assets/vendors/moment/min/moment.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- CkEditor -->
    <script src="<?php echo base_url(); ?>assets/vendors/ckeditor/ckeditor.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="<?php echo base_url(); ?>assets/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- Datatables -->
    <script src="<?php echo base_url(); ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url(); ?>assets/build/js/custom.min.js"></script>
    <!-- combobox -->
    <script src="<?php echo base_url(); ?>assets/combobox-master/combobox.js"></script>
    <!-- pnotify -->
    <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="<?php echo base_url(); ?>assets/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    <script>
    $('#myDatepicker2').datetimepicker({
      format: 'DD-MM-YYYY'
    });
    </script>
    <script type="text/javascript">
    $('#notifikasi').slideDown('slow').delay(5000).slideUp('slow');
    </script>
    <script>
    function notifTambahPasien(message){
      if(message == "success"){
        new PNotify({
                        title: 'Success',
                        text: 'Pasien baru telah berhasil ditambahkan!',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
      }else{
        new PNotify({
                        title: 'Regular Failed',
                        text: 'Pasien baru gagal ditambahkan!!',
                        type: 'Error',
                        styling: 'bootstrap3'
                    });
      }
    }

    function notifEditProfile(message){
      if(message == "success"){
        new PNotify({
                        title: 'Success',
                        text: 'Data Profile berhasil diubah!',
                        type: 'success',
                        styling: 'bootstrap3'
                    });
      }else{
        new PNotify({
                        title: 'Regular Failed',
                        text: 'Data Profile gagal diubah!',
                        type: 'Error',
                        styling: 'bootstrap3'
                    });
      }
    }

    </script>
  </body>
</html>
