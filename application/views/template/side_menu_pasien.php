<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="<?php echo base_url(); ?>" class="site_title">EMR</a>
    </div>
    <br>
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <h3>Pasien MENU</h3>
        <ul class="nav side-menu">
            <li><a href="<?php echo base_url(); ?>index.php/profile/"><i class="fa fa-user"></i>Profile</a>
            </li>
            <li><a href="<?php echo base_url('index.php/C_rekam_medis/'); ?>"><i class="fa fa-h-square"></i>Rekam Medis</a>
            </li>
        </ul>
    </div>
</div>
</div>
</div>

<!-- top navigation -->
<div class="top_nav">
  <div class="nav_menu">
    <nav>
      <div class="nav toggle">
        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li class="">
          <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <?php echo $this->session->userdata('nama'); ?>
            <span class=" fa fa-angle-down"></span>
          </a>
          <ul class="dropdown-menu dropdown-usermenu pull-right">
            <li><a href="<?php echo base_url(); ?>index.php/User/Setting/">Setting</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/Login/logout/"><i class="fa fa-sign-out pull-right"></i>Log Out</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>
</div>
<!-- /top navigation -->
